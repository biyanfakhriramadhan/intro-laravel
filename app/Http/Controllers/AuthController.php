<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        $data = ['title' => "Form Daftar"];
        return view('register', $data);
    }

    // welcome
    public function welcome(request $isi)
    {
        $first = $isi->first;
        $last = $isi->last;
        $data = ['title' => "Selamat Datang!"];
        return view('welcome', $data, compact('first', 'last'));
    }
}
